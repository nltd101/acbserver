
var express = require('express');
var app = express.Router();
var controller = require('../controllers/user.controller');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false })

var jsonParser = bodyParser.json();

app.get('/a/:username',controller.get);
app.get('/search',controller.search);
app.post('/rename', jsonParser, controller.rename);



module.exports=app;